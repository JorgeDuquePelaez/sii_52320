//Autor: Jorge Duque Peláez

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("fifo",0777);
	int fd=open("fifo", O_RDONLY);//Definimos la tubería como lectura porque el logger solo va a leer datos
	if(fd < 0){
		perror("Fallo en la apertura de la tuberia");
		return(1);
	}
	int aux;
	char buff[200];
	while(1)
	{
		aux=read(fd,buff,sizeof(buff));
		if(aux < 0){
			perror("Fallo en la lectura");
			return(1);
		}
		if(aux == 0)//Fin de lectura de datos
		{
			perror("Fin de programa");
			return(1);
		}
		printf("%s\n", buff);

	}
	close(fd);//Cerramos el descriptor
	unlink("fifo");//Cerramos la fifo
	return 0;
}
